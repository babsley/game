'use strict';

const config = require('../config.json');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);
const gulp = require('gulp');

module.exports = (options) => {
    return () => {
        return $.combiner.obj(
            $.rollup({entry: config.paths.src + config.paths.scripts.main, sourceMap: true, format: 'es'}),
            $.source(config.concat.js.outputFileName),
            $.buffer(),
            $.if(config[$.yargs.argv.env].scripts.sourcemaps, $.sourcemaps.init(config.sourcemaps.init)),
            $.babel(config.babel),
            $.uglify(),
            $.rename({suffix: '.min'}),
            $.if(config[$.yargs.argv.env].scripts.sourcemaps, $.sourcemaps.write(config.sourcemaps.write)),
            $.connect.reload(),
            gulp.dest(config[$.yargs.argv.env].dest + config.paths.scripts.dest)
        ).on('error', $.notify.onError(function (err) {
                return {
                    title: options.taskName,
                    message: err.message
                }
            })
        );
    }
};