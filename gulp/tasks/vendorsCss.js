'use strict';

const config = require('../config.json');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);

module.exports = (options) => {
    return () => {
        return $.combiner.obj(
            gulp.src(config.vendors.css),
            $.autoprefixer(config.autoprefixer),
            $.if(config[$.yargs.argv.env].vendorsCss.concat, $.concat(config.concat.vendorsCss.outputFileName)),
            $.if(config[$.yargs.argv.env].vendorsCss.clean, $.cleanCss()),
            gulp.dest(config[$.yargs.argv.env].dest + config.paths.styles.dest)
        ).on('error', $.notify.onError(function (err) {
                return {
                    title: options.taskName,
                    message: err.message
                }
            })
        );
    }
};