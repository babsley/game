'use strict';

const config = require('../config.json');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);

module.exports = (options) => {
    return () => {
        return $.combiner.obj(
            gulp.src(config.vendors.js),
            $.if(config[$.yargs.argv.env].vendorsJs.concat, $.concat(config.concat.vendorsJs.outputFileName)),
            $.if(config[$.yargs.argv.env].vendorsJs.uglify, $.uglify()),
            gulp.dest(config[$.yargs.argv.env].dest + config.paths.scripts.dest)
        ).on('error', $.notify.onError(function (err) {
                return {
                    title: options.taskName,
                    message: err.message
                }
            })
        );
    }
};