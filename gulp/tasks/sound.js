'use strict';

const config = require('../config.json');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);
const gulp = require('gulp');

module.exports = (options) => {
    return () => {
        return $.combiner.obj(
            gulp.src(config.paths.src + config.paths.sound.src),
            $.newer(config[$.yargs.argv.env].dest + config.paths.sound.dest),
            gulp.dest(config[$.yargs.argv.env].dest + config.paths.sound.dest)
        ).on('error', $.notify.onError(function (err) {
                return {
                    title: options.taskName,
                    message: err.message
                }
            })
        );
    }
};