'use strict';

const config = require('../config.json');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);


module.exports = (options) => {
    return () => {
        return $.combiner.obj(
            gulp.src('./src/styles/app.scss'),
            $.if(config[$.yargs.argv.env].styles.sourcemaps, $.sourcemaps.init(config.sourcemaps.init)),
            $.autoprefixer(config.autoprefixer),
            $.sass(config.sass),
            $.mergeMediaQueries(),
            $.if(config[$.yargs.argv.env].styles.concat, $.concat(config.concat.css.outputFileName)),
            $.if(config[$.yargs.argv.env].styles.clean, $.cleanCss()),
            $.if(config[$.yargs.argv.env].styles.sourcemaps, $.sourcemaps.write(config.sourcemaps.write)),
            $.connect.reload(),
            gulp.dest(config[$.yargs.argv.env].dest + config.paths.styles.dest)
        ).on('error', $.notify.onError(function (err) {
                return {
                    title: options.taskName,
                    message: err.message
                }
            })
        );
    }
};