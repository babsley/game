'use strict';

const config = require('../config.json');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);

module.exports = (options) => {
    return () => {
        return $.combiner.obj(
            gulp.src(config.vendors.fonts + '/{*.eot,*.svg,*.ttf,*.woff,*.woff2}'),
            gulp.dest(config[$.yargs.argv.env].dest + config.paths.fonts.dest)
        ).on('error', $.notify.onError(function (err) {
                return {
                    title: options.taskName,
                    message: err.message
                }
            })
        );
    }
};