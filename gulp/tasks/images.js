'use strict';

const config = require('../config.json');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);
const gulp = require('gulp');

module.exports = (options) => {
    return () => {
        return $.combiner.obj(
            gulp.src(config.paths.src + config.paths.images.src),
            $.newer(config[$.yargs.argv.env].dest + config.paths.images.dest),
            $.imagemin(),
            gulp.dest(config[$.yargs.argv.env].dest + config.paths.images.dest)
        ).on('error', $.notify.onError(function (err) {
                return {
                    title: options.taskName,
                    message: err.message
                }
            })
        );
    }
};