'use strict';

const config = require('../config.json');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);


module.exports = (options) => {
    return () => {
        return $.combiner.obj(
            gulp.src(config.paths.src + config.paths.pages.src),
            $.inject(
                gulp.src(
                    config[$.yargs.argv.env].dest + config.paths.scripts.dest + config.inject.pages.scripts.first,
                    {read: false}),
                {
                    starttag: '<!-- inject:first:{{ext}} -->',
                    relative: true,
                    ignorePath: '../' + config[$.yargs.argv.env].dest + '/'
                }
            ),
            $.inject(
                gulp.src([
                        config[$.yargs.argv.env].dest + config.paths.scripts.dest + '/*.js',
                        '!' + config[$.yargs.argv.env].dest + config.paths.scripts.dest + config.inject.pages.scripts.vendors,
                        config[$.yargs.argv.env].dest + config.paths.styles.dest + '/*.css',
                        '!' + config[$.yargs.argv.env].dest + config.paths.styles.dest + config.inject.pages.styles.vendors,
                    ],
                    {read: false}),
                {
                    relative: true,
                    ignorePath: '../' + config[$.yargs.argv.env].dest + '/'
                }
            ),
            $.inject(
                gulp.src([
                        config[$.yargs.argv.env].dest + config.paths.scripts.dest + config.inject.pages.scripts.main,
                        '!' + config[$.yargs.argv.env].dest + '**/*.js',
                        config[$.yargs.argv.env].dest + config.paths.styles.dest + config.inject.pages.styles.main,
                        '!' + config[$.yargs.argv.env].dest + '**/*.css',
                    ],
                    {read: false}),
                {
                    starttag: '<!-- inject:main:{{ext}} -->',
                    relative: true,
                    ignorePath: '../' + config[$.yargs.argv.env].dest + '/'
                }
            ),
            $.connect.reload(),
            gulp.dest(config[$.yargs.argv.env].dest)
        ).on('error', $.notify.onError(function (err) {
                return {
                    title: options.taskName,
                    message: err.message
                }
            })
        );
    }
};