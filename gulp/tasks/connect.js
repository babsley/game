'use strict';

const config = require('../config.json');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);
const gulp = require('gulp');

module.exports = (options) => {
    return () => {
        return $.connect.server({
            root: config[$.yargs.argv.env].dest,
            port: config.connect.port,
            livereload: config.connect.livereload,
            middleware: function (connect, opt) {
                return [
                    $.proxy(config.connect.proxyUrl, config.connect.proxy)
                ]
            }
        });
    }
};