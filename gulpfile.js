'use strict';

const config = require('./gulp/config.json');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')(config.gulpLoadPlugins);

$.yargs.default('env', 'development');

// require task
function lazyRequireTask(taskName, options) {
    options = options || {};
    options.taskName = taskName;

    if (!options.path) {
        options.path = config.paths.tasksPath + taskName;
    }

    gulp.task(taskName, function (callback) {
        let task = require(options.path).call(this, options);
        return task(callback);
    });
}

// clean task
lazyRequireTask('clean');

// images task
lazyRequireTask('images');

// sound task
lazyRequireTask('sound');

// pages task
lazyRequireTask('pages');

// styles task
lazyRequireTask('styles');

// vendprs styles task
lazyRequireTask('vendorsCss');

// scripts task
lazyRequireTask('scripts');

// vendprs scripts task
lazyRequireTask('vendorsJs');

// vendprs fonts task
lazyRequireTask('vendorsFonts');

// connect
lazyRequireTask('connect');


//watch task
gulp.task('watch', () => {
    gulp.watch(config.paths.src + config.paths.pages.src, ['pages']);
    gulp.watch(config.paths.src + config.paths.styles.src, ['styles']);
    gulp.watch(config.paths.src + config.paths.scripts.src, ['scripts']);
});

// build task
gulp.task('build', (callback) => {
    $.runSequence('clean', ['sound', 'images', 'styles', 'scripts', 'vendorsCss', 'vendorsJs', 'vendorsFonts'], 'pages', callback);
});

// default task
gulp.task('default', () => {
    $.runSequence(['build'], 'watch', 'connect');
});



