class Camera {
    constructor(ctx, size, mapSize) {
        this.ctx = ctx;
        this.size = size;
        this.mapSize = mapSize;
        this.pos = [0, -1000];
        this.ctx.translate(this.pos[0], this.pos[1]);
    }

    setEntity(entity) {
        if (this.entity) {
            return;
        }

        this.entity = entity;
    }


    onChange(data) {
        if (data.x) {
            if (this.entity.pos[0] + this.entity.width < this.size[0] / 2) {
                this.ctx.translate(-this.pos[0], 0);
                this.pos[0] = 0;
            } else if (this.entity.pos[0] + this.entity.width > this.mapSize[0] - this.size[0] / 2) {
                this.ctx.translate(-(this.pos[0] + this.mapSize[0] - this.size[0]), 0);
                this.pos[0] = (this.mapSize[0] - this.size[0]) * -1;
            } else {
                this.ctx.translate(data.x, 0);
                this.pos[0] += data.x;
            }
        }
        if (data.y) {
            this.ctx.translate(0, -data.y);
            this.pos[1] -= data.y;
        }
    }
}

export default Camera;