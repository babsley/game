'use strict';

export function isDown(obj) {
    document.addEventListener('keydown', function (e) {
        // console.info(e.keyCode);
        obj[e.keyCode] = true;
    });

    document.addEventListener('keyup', function (e) {
        obj[e.keyCode] = false;
    });
}