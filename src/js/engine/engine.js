'use strict';
import { isDown } from './key-events';
import Camera from './camera';

class Engine {
    constructor(ctx, size) {
        this._entities = [];
        this._lastTime = null;
        this._entId = 0;
        this._keyDown = {};
        this._ctx = ctx;
        this._mapSize = [6000, 3000]; // test values, need map size
        this.cam = new Camera(ctx, size, this._mapSize);
    }

    // updates each entity inner state
    _update(dt) {
        this._entities.forEach(entity => {
            // entity update have to be function
            if (!entity.update || typeof entity.update !== 'function') {
                throw 'entity.update have to be function';
            }

            entity.update(dt);
        });
    }

    // renders each entity on canvas
    _render() {
        this._ctx.clearRect(0, 0, this._mapSize[0], this._mapSize[1]);
        this._entities.forEach(entity => {
            if (!entity.render || typeof entity.render !== 'function') {
                throw 'entity.update have to be function';
            }

            entity.render(this._ctx);
        });

    }

    _loop() {
        const now = Date.now();
        const dt = (now - this._lastTime) / 1000.0; // difference between now time and last time update

        if (this._entities.length) {
            this._update(dt);
            this._render();
        }

        this._lastTime = now;
        requestAnimationFrame(() => this._loop());
    }

    isDown(key) {
        return this._keyDown[key] || false;
    }

    // entity must have a property "zIndex"
    addEntity(entity) {
        if (!entity.zIndex) {
            throw 'entity must have property "zIndex"';
        }
        entity._id = this._entId++;

        this._entities.push(entity);
        this._entities.sort((a, b) => a.zIndex - b.zIndex);
    }

    removeEntity(entity) {
        this._entities.splice(this._entities.findIndex((el) => el._id === entity._id), 1);
    }

    init() {
        isDown(this._keyDown);
        this._lastTime = Date.now();
        this._loop();
    }

    // returns canvas size [horizontal, vertical]
    // we can get one property
    // for example get horizontal => game.getSize(0);
    //             get vertical   => game.getSize(1);
    getSize(coords) {
        if (coords === undefined) {
            return [this._mapSize[0], this._mapSize[1]];
        }

        return this._mapSize[coords];
    }

    getCtx() {
        return this._ctx;
    }
}


export default Engine;