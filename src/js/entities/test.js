class TestEntity {
    constructor(x, y, index) {
        this.pos = [x, y];
        this.width = 200;
        this.height = 20;
        this.zIndex = index;
    }

    update() {

    }

    render(ctx) {
        ctx.save();
        ctx.fillStyle = '#f3f3f3';
        ctx.fillRect(this.pos[0], this.pos[1], this.width, this.height);
        ctx.restore();
    }

}

export default TestEntity;