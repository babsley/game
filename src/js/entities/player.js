class Player {
    constructor(game) {
        this.game = game;
        this.zIndex = 100;
        this.width = 100;
        this.height = 100;
        this.speed = 1000;
        this.vector = [this.speed, this.speed];
        this.mapSize = game.getSize();
        this.pos = [0, this.mapSize[1] / 2 - this.height / 2];
        this.game.cam.setEntity(this);
    }

    update(dt) {
        // arrow right
        if (this.game.isDown('39')) {
            let distance = this.vector[0] * dt;

            if (this.pos[0] + this.width >= this.mapSize[0]) {
                this.pos[0] = this.mapSize[0] - this.width;

                return;
            }

            this.game.cam.onChange({x: -distance});
            this.pos[0] += distance;
        }

        // arrow left
        if (this.game.isDown('37')) {
            let distance = this.vector[0] * dt;

            if (this.pos[0] <= 0) {
                this.pos[0] = 0;
                return;
            }

            this.game.cam.onChange({x: distance});
            this.pos[0] -= distance;
        }

        // arrow up
        if (this.game.isDown('38')) {
            let distance = this.vector[1] * dt

            if (this.pos[1] <= 0) {
                this.pos[1] = 0;
                return;
            }

            this.game.cam.onChange({y: -distance});
            this.pos[1] -= distance;
        }

        // arrow down
        if (this.game.isDown('40')) {
            let distance = this.vector[1] * dt

            if (this.pos[1] + this.height >= this.mapSize[1]) {
                this.pos[1] = this.mapSize[1] - this.height;
                return;
            }

            this.game.cam.onChange({y: distance});
            this.pos[1] += distance;
        }

        if (this.game.isDown('32')) {
            this.vector[0] = this.speed * 3;
        } else {
            this.vector[0] = this.speed;
        }

    }

    render(ctx) {
        ctx.save();
        ctx.fillStyle = '#666';
        ctx.fillRect(this.pos[0], this.pos[1], this.width, this.height);
        ctx.restore();
    }
}

export default Player;
