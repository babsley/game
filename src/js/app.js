import Engine from './engine/engine';

import Player from './entities/player';
import TestEntity from './entities/test';


const canvasSize = [1920, 1080];
const canvas = document.createElement('canvas');
const ctx = canvas.getContext('2d');

canvas.width = 1920;
canvas.height = 1080;
document.body.appendChild(canvas);

const game = new Engine(ctx, canvasSize);
const player = new Player(game);

game.addEntity(player);

let mapSize = game.getSize();
for (let i = 0; i < 6000; i++) {
    game.addEntity(new TestEntity(i * 200, mapSize[1] / 2 + (i % 2 ? 0 : 30), i % 2 ? 200 : 1));
}

game.init();
